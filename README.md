# Gaussian Covariances for CosmoSIS

The code in this repository is a CosmoSIS module designed to generate fast Fourier space covariance matrixes for 3x2pt data.
It's intended to work as-is, plugged into whichever CosmoSIS pipeline you happen to be using. 
That said, there are a few steps in the process and the code isn't necessarily very transparent. To try to ease usage a bit the repo also contains a simple demo, which you can run by following the instructions below.

Contributors: 
Simon Samuroff, Donnacha Kirk, Jack Elvin-Poole

Used in:

* https://arxiv.org/abs/1607.07910

Dependencies:

* A working install of CosmoSIS

Contact: ssamurof@andrew.cmu.edu

## Instructions

1. Source CosmoSIS in the home directory of your main install

2. Navigate to cosmosis_gauss_cov/code/ and run $ bash run-example. This should do the following in turn:

* Call CosmoSIS once to produce mock Cl data. The parameters are fixed at the values in values-fixed.ini, and the result should be a pickle file called example-3x2pt-data.pi

* Calls CosmoSIS again, reading in that data vector of ndat points. Theory Cls are computed, to which shot noise is added before they're used to generate a ndat x ndat covariance matrix. This is saved as a text file, example-3x2pt-cov.txt

* Calls CosmoSIS a third time, reading in both the data vector and the covariance matrix. Evaluates the likelihood at the input cosmology (and so returns zero, by contruction)


## Overview 

There are two extra modules used here:

a. generate_observable_cls

and

b. cl_likelihood

The source code is located (obviously) in cosmosis_gauss_cov/code. 
The first of these essentially bins the theory Cls adds shot noise (if running in covariance generation mode).
The ini file parameters are

> nlbin_shear : number of ell bins for the shear-shear Cls

> nlbin_ggl : number of ell bins for the galaxy-shear Cls

> nlbin_pos : number of ell bins for the galaxy-galaxy Cls

> lmax_shear : maximum multipole to use in the shear-shear Cls

> lmin_shear : minimum multipole to use in the shear-shear Cls

> lmax_ggl : maximum multipole to use in the galaxy-shear Cls

> lmin_ggl : minimum multipole to use in the galaxy-shear Cls

> lmax_pos : maximum multipole to use in the galaxy-galaxy Cls

> lmin_pos : minimum multipole to use in the galaxy-galaxy Cls

> shear : whether to do the shear-shear Cls

> ggl : whether to do the galaxy-shear Cls

> position : whether to do the galaxy-galaxy Cls

> intrinsic_alignments : whether to do the intrinsic-shear and intrinsic-intrinsic Cls

> magnification : whether to do number count magnification Cls

> bias : apply multiplicative bias (unnecessary if using the shear_m_bias module for this)?

> m_per_bin : apply a separate value per bin?

> angular_frequency_bins : whether to apply ell binning

> window  : if so what sort of window function to use ('tophat' or 'delta' work here)


The second module handles the covariance calculation and subsequent likelihood evaluation. Its parameters are:

> fixed_covariance : whether or not to do the covariance calculation

> shear_sample : name to look for the source n(z) under

> LSS_sample : name to look for the lens n(z) under

> auto_zbins : include auto-bin pairs (almost certainly should be True)?

> cross_zbins : include cross-bin pairs?

> shear : include shear-shear?

> position : include galaxy-galaxy?

> ggl: include galaxy-shear?

> data : Data vector to use for the likelihood call

> output : Text file to which to save the covariance matrix, if fixed_covariance=F

> covariance : Text file from which to read the covariance matrix, if fixed_covariance=T

## Other notes

* You can set the shot noise and area to use in the covariance calculation in the ini file. At the top include something like

> [runtime]
>
> global = source lens

(with 'source' and 'lens' being the names of the galaxy samples used). Then create new sections of the form

> [sample_name]
>
> ngal = n1 n2 n3 n4
>
> shape_dispersion = sig_e
>
> area = area

where n1,n2,n3,n4 are the number densities in each of the tomographic bins used for this sample. If a single number is given (and multiple redshift bins are being used) that value will be taken as the total, and divided equally between the bins.
In units of gal arcmin^{-2}. The ellipticity dispersion is just a single number.
The area is the survey area in square degrees. No treatment of masking.